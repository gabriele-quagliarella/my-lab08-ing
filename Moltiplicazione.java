package it.unimi.di.se.lab08;

import java.util.Stack;

public class Moltiplicazione extends Operation {

    public Moltiplicazione(){
        this.op = "*";
    }

    @Override
    public void operation(Stack<Integer> pila) {
        int op1;
        int op2;
        if(!pila.empty()){
            op1 = pila.pop().intValue();
            if(!pila.empty()){
                op2 = pila.pop().intValue();
                pila.push(op1 * op2);
            }
            else{
                throw new IllegalArgumentException();
            }
        }else{
            throw new IllegalArgumentException();
        }
    }
}
