package it.unimi.di.se.lab08;

public class CalculatorFactory {
    public Calculator createSimpleCalculator(String expression) {
        return new SimpleCalculator(expression);
    }

    public Calculator createFaultTolleranceCalculator(String expression) {
        return new FaultTolleranceCalculator(expression);
    }
}
