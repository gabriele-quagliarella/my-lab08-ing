package it.unimi.di.se.lab08;

import java.util.Stack;

public abstract class Operation {
    protected String op;
    protected Operation successor = null;

    abstract public void operation(Stack<Integer> pila);

    public void setSuccessor(Operation o){
        this.successor = o;
    }

    public Operation request(String operand){
        if (this.op.equals(operand)){
            return this;
        }else{
            if (this.successor == null){
                return null;
            }
            return this.successor.request(operand);
        }
    }
}
