package it.unimi.di.se.lab08;

public abstract class Calculator {

    protected String expression;
    protected Operation sum;
    protected Operation sub;
    protected Operation mul;
    protected Operation div;


    public Calculator(String s) {

        this.expression = s;
        this.sum = new Somma();
        this.sub = new Sottrazione();
        this.mul = new Moltiplicazione();
        this.div = new Divisione();

        this.sum.setSuccessor(this.sub);
        this.sub.setSuccessor(this.mul);
        this.mul.setSuccessor(this.div);
    }

    public abstract int getResult();
}
