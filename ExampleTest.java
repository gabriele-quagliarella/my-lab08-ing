package it.unimi.di.se.lab08;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import static org.assertj.core.api.Assertions.*;


public class ExampleTest {

    @Rule
    public Timeout globalTimeout = Timeout.seconds(2);

    @Test
    public void simpleSumTest() {
        Calculator calc = new SimpleCalculator("5 4 +");
        assertThat(calc.getResult()).isEqualTo(9);
    }

    @Test
    public void characterExceptionTest() {
        Calculator calc1 = new SimpleCalculator("5 4 +");
        assertThat(calc1.getResult()).isEqualTo(9);

        Calculator calc2 = new SimpleCalculator("5 4");
        assertThatThrownBy(() -> {
            calc2.getResult();})
                .isInstanceOf(IllegalArgumentException.class);

        Calculator calc3 = new SimpleCalculator("5 4 + +");
        assertThatThrownBy(() -> {
            calc3.getResult();})
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    public void simpleMulTest(){
        Calculator calc1 = new SimpleCalculator("5 4 *");
        assertThat(calc1.getResult()).isEqualTo(20);

        Calculator calc2 = new SimpleCalculator("7 6 + 5 * 4 3 * +");
        assertThat(calc2.getResult()).isEqualTo(77);
    }

    @Test
    public void multipleOperationTest(){
        Calculator calc1 = new SimpleCalculator("10 2 * 3 5 - + 2 /");
        assertThat(calc1.getResult()).isEqualTo(9);
    }

    @Test
    public void multipleCalcTest(){
        Calculator simpleCalc = new CalculatorFactory().createSimpleCalculator("5 +a 4 + ?");
        Calculator faultCalc = new CalculatorFactory().createFaultTolleranceCalculator("5 +a 4 + ?");

        assertThatThrownBy(() -> {
            simpleCalc.getResult();})
                .isInstanceOf(IllegalArgumentException.class);

        assertThat(faultCalc.getResult()).isEqualTo(9);

    }
}
