package it.unimi.di.se.lab08;

import java.util.Stack;

public class SimpleCalculator extends Calculator{

    public SimpleCalculator(String s) {
        super(s);
    }

    @Override
    public int getResult() {
        String[] parsedString = expression.split(" ");
        Operation op;
        int result;

        Stack<Integer> pila = new Stack<Integer>();

        for (String c: parsedString){
            if(c.matches("[0-9]+")){
                pila.push(Integer.parseInt(c));
            }
            else{
                op = this.sum.request(c);
                if (op != null){
                    op.operation(pila);
                }else {
                    throw new IllegalArgumentException("Operazione non supportata");
                }
            }
        }

        if (!pila.isEmpty()){
            result = pila.pop().intValue();
            if(pila.isEmpty()){
                return result;
            }
        }
        throw new IllegalArgumentException();
    }
}
